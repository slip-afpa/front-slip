var url;
url ='';

online = false;
urlUser = localStorage.getItem("urlLocalHost");
console.log(urlUser)
if (urlUser == null) {
    url = 'http://10.115.57.140:8888';
} else {
    url = urlUser;
}

// Call
async function getData(path) {
    try {
        let response = await fetch(url + path);
        let info = await response.json()
        return info;
    }
    catch (error) {
        console.log('error line 24 toto')
    }
}

function setHtml(data) {
    let info = data;
    if (info == 'OK') {
        document.getElementById("BDDStateOK").hidden = false;
        document.getElementById("BDDStateKO").hidden = true;
        document.getElementById("BDDStateOKMessage").innerHTML = "<p>Database</p>";
    } else {
        document.getElementById("BDDStateKO").hidden = false;
        document.getElementById("BDDStateOK").hidden = true;
        document.getElementById("BDDStateOKMessage").innerHTML = "<p>Database</p>"
    }
    return info;
}

var fetchBDD = async function () {
    try {
        const response = await getData("/checkBDD")
        setHtml(response);
    } catch (error) {
        console.log('xxxx')
        document.getElementById("BDDStateKO").hidden = false;
        document.getElementById("BDDStateOK").hidden = true;
        document.getElementById("BDDStateOKMessage").innerHTML = "<p>Database</p>"
    }
}

function setHtml(data) {
    if (data == 'OK') {
        document.getElementById("BDDStateOK").hidden = false;
        document.getElementById("BDDStateKO").hidden = true;
        document.getElementById("BDDStateOKMessage").innerHTML = "<p>Database</p>";
    } else {
        document.getElementById("BDDStateKO").hidden = false;
        document.getElementById("BDDStateOK").hidden = true;
        document.getElementById("BDDStateOKMessage").innerHTML = "<p>Database</p>"
    }
}

const telltime = async function () {
    fetch(url + "/getInfo")
        .then(response => response.json())
        .then(response => afficheLHeure(response));
}

function afficheLHeure(dico) {
    let dateEnume = dico['date'].split(' ');
    let days = dateEnume[0].split('-');
    let hours = dateEnume[1].split(':');
    let calendar = ""
    let horloge = ""
    for (let i = 0; i < days.length; i++) {
        calendar += "<span>" + days[i] + "</span>"
        if ((i + 1) < days.length) {
            calendar += "<span>/</span>"
        }
    }
    for (let i = 0; i < hours.length; i++) {
        horloge += "<span>" + hours[i] + "</span>"
        if ((i + 1) < hours.length) {
            horloge += "<span>:</span>"
        }
    }
    document.getElementById("affichageDate").innerHTML = calendar;
    document.getElementById("affichageHeure").innerHTML = horloge;
}


getData("/machine").then(
    function (value) {
        let machine = value['hostname']
        document.getElementById("affichageInfo").innerHTML = `You are connected in : <label style="color:#19ccd5">` + machine + '</label>';
    },
    function (error) { document.getElementById("affichageInfo").innerHTML = `API est Hors ligne `; }
)

setInterval(function () {
    telltime().catch()
}, 1000)




fetchBDD()