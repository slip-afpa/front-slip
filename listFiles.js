urlUser = localStorage.getItem("urlLocalHost");

var colorBefore = "";

var poubelle = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>'

var restore = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-recycle" viewBox="0 0 16 16"><path d="M9.302 1.256a1.5 1.5 0 0 0-2.604 0l-1.704 2.98a.5.5 0 0 0 .869.497l1.703-2.981a.5.5 0 0 1 .868 0l2.54 4.444-1.256-.337a.5.5 0 1 0-.26.966l2.415.647a.5.5 0 0 0 .613-.353l.647-2.415a.5.5 0 1 0-.966-.259l-.333 1.242-2.532-4.431zM2.973 7.773l-1.255.337a.5.5 0 1 1-.26-.966l2.416-.647a.5.5 0 0 1 .612.353l.647 2.415a.5.5 0 0 1-.966.259l-.333-1.242-2.545 4.454a.5.5 0 0 0 .434.748H5a.5.5 0 0 1 0 1H1.723A1.5 1.5 0 0 1 .421 12.24l2.552-4.467zm10.89 1.463a.5.5 0 1 0-.868.496l1.716 3.004a.5.5 0 0 1-.434.748h-5.57l.647-.646a.5.5 0 1 0-.708-.707l-1.5 1.5a.498.498 0 0 0 0 .707l1.5 1.5a.5.5 0 1 0 .708-.707l-.647-.647h5.57a1.5 1.5 0 0 0 1.302-2.244l-1.716-3.004z"/></svg>'

var ipContainer = [
    'http://10.115.57.140:8888',
    'http://10.115.57.137:8888',
    'http://10.115.57.82:8888',
    'http://10.115.57.130:8888',
    'http://10.115.57.92:8888'
]

function openModal() {
    monHtmlDeMerde = '<option selected>Select your url</option>';
    for (let i = 0; i < ipContainer.length; i++) {
        monHtmlDeMerde += '<option value="'+ ipContainer[i] +'">' + ipContainer[i] + '</option>';
    }
    document.getElementById('optionUrl').innerHTML = monHtmlDeMerde;
    if (urlUser == null) {
        url = 'http://10.115.57.140:8888';
    } else {
        url = urlUser;
    }
    document.getElementById('currentUrl').innerHTML = url;
}

function openValidate() {
    localStorage.setItem("urlLocalHost", document.getElementById("optionUrl").value);
    location.reload();
}
// fonction utilisée par maquetteRework.html
function lineS(elem, color) {
    let button = ''
    elem.del == 1 ? button = '<td><button class="btn btn-outline-success" onclick="RestoreFilesIn(' + elem.id + ')">' + restore + '</button></td>' : button = '<td><button class="btn btn-outline-warning" onclick="DelFilesIn(' + elem.id + ')">' + poubelle + '</button></td>';
    document.getElementById('listFile').innerHTML += '<tr><td>' +
        elem.author + '</td><td>' +
        elem.name + '</td><td>' +
        elem.date + '</td><td>' + button + '</tr>';
}


function affLineObserver( elem, color)
{
    let button = ''
    elem.del == 1 ? button = '<td><button class="btn btn-outline-success" onclick="RestoreFilesIn(' + elem.id + ')">' + restore + '</button></td>' : button = '<td><button class="btn btn-outline-warning" onclick="DelFilesIn(' + elem.id + ')">' + poubelle + '</button></td>';

    document.getElementById('listFile').innerHTML += '<tr><td>' +
        elem.name + '</td><td>' +
        elem.repIN + '</td><td>' +
        elem.repPROD+ '</td><td>' + button + '</tr>';

}



// function lineOri(elem, color) {
//     document.getElementById('listFile').innerHTML += '<li>' + '<b>author</b> : ' + elem.author + '    name : ' + elem.name + '    date : ' + elem.date +
//         '<button onclick="DelFilesIn(' + elem.id + ')">effacer</button>'
// // fonction utilisée par maquette.html
// function lineOri2(elem, color, delButton = true, restoreButton = false) {
//     document.getElementById('listFile').innerHTML += '<li>' + elem.author + ' ' + elem.name + ' ' + elem.date + (
//         delButton ? '<button onclick="DelFilesIn(' + elem.id + ')">effacer</button>' : '') + (
//             restoreButton ? '<button onclick="RestoreFilesIn(' + elem.id + ')">' + restore + '</button>' : '')
//         + '</li> <hr>';
// }


// Paramètres path=url de service, les paramètres sont mis en place dans la page html.
async function getDataAndList(path, color, lineHook = lineS, delButton = true, restoreButton = false) {

    let myjson = await getData(path);
    let filter = "";
    console.log('xxx');
    console.log(myjson);
    document.getElementById('listFile').innerHTML = '';
    for (let i = 0; i < myjson.length; i++) {
        lineHook(myjson[i], color, delButton, restoreButton);
    }
    //console.log(tab)
    //console.log(filter)
    document.getElementById("myTable").hidden = false;
    document.getElementById("filter").hidden = false;
    document.getElementById("formulaire").hidden = true; 
    // supprime la classe initialiser précédemment
    colorBefore != "" ? document.getElementById('myTable').classList.remove(colorBefore) : colorBefore = "";
    // ajoute la classe correspondant aux boutons
    document.getElementById('myTable').classList.add(color);
    document.getElementById('checkBox').innerHTML = filter;
    colorBefore = color;
}


function DelFilesIn(id) {
    fetch(url + "/DelFilesIn/" + id)
        .then(response => response.json());
}


function RestoreFilesIn(id) {
    fetch(url + "/RestoreFilesIn/" + id)
        .then(response => response.json());
}

//interface pour vielle version
function getFilesIn() {
    getDataAndList('/getFilesIn', '', lineOri2);
}
function getFilesProd() {
    getDataAndList('/getFilesProd', '', lineOri2);
}
function getFilesError() {
    getDataAndList('/getFilesErr', '', lineOri2);
}
function getFilesDone() {
    getDataAndList('/getFilesDone', '', lineOri2);
}
function getFilesDel() {
    getDataAndList('/getFilesDel', '', lineOri2, false, true);
}

function getListObserver() {
    getDataAndList('/getObserver', '', affLineObserver, false, true);
}

function test(){

    let nom = document.getElementById('nom').value;
    let pathIN = document.getElementById('pathIn').value;
    let pathPROD = document.getElementById('pathProd').value;
    let pathERR = document.getElementById('pathErr').value;
  
}

function openFormulaire(){
    document.getElementById("myTable").hidden = true;
    //$("#myTable").hide();
    document.getElementById("formulaire").hidden = false;
    //$("#formulaire").show(); 
}



